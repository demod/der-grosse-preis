<?php
session_start();

//classes
require_once "class/teams.class.php";
require_once "functions/mysql.php";
require_once "functions/teams.php";
require_once "class/question.class.php";
require_once "class/overview.class.php";
require_once "functions/common.php";


//templating header
include "views/header.php";


if (!empty($_GET['question'])){
    $question = new Question($_GET['question']);
}else{
   Overview::showOverview();
}

if (!empty($_GET['newgame'])){
    clearGame();
}

//templating status bar
include "views/status.php";

//templating footer
include "views/footer.php";