<?php

class Question {

    private $id;
    private $question;
    private $answer;
    private $statement;
    private $topic;
    private $points;
    private $won_group;

    public function __construct($id)
    {
        $this->id = $id;
        $this->getData();
        if (empty($this->won_group) || ($this->won_group == "0")){

            if (!empty($_GET['action']) && !empty($_GET['team'])){
                if ($_GET['action'] == "save"){
                    $this->save();
                }
            }else{
                $this->printQuestion();
            }

        }else{
            echo "bereits genutzt";
        }

    }

    private function getData(){
        global $conn;

        $sql = "SELECT * FROM questions WHERE id = " . $this->id;
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {

                $this->question = $row["question"];
                $this->answer = $row["answer"];
                $this->statement = $row["statement"];
                $this->topic = $row["topic"];
                $this->points = $row["points"];
                $this->won_group = $row["won_group"];

            }
        }
    }

    private function printQuestion(){
        $html = '<div class="questionContainer"><div class="question"><p>';
        $html .= $this->question;
        $html .= '</p></div><hr /><div class="countdown"></div>';
        $html .= $this->printAnswer();
        $html .= $this->printStatement();
        $html .= '</div>';

        echo $html;
    }

    private function printAnswer(){
        $html = '<div class="answer"><span>Antwort:</span>&nbsp;';
        $html .= $this->answer;
        $html .= '</div>';

        return $html;
    }

    private function printStatement(){
        $html = '<div class="statement">';
        $html .= $this->statement;
        $html .= '</div>';

        return $html;
    }

    private function save(){
        global $conn;

        $this->won_group = $_GET['team'];
        $sql = "UPDATE questions SET won_group = '".$this->won_group."' WHERE id = " . $this->id;
        $result = $conn->query($sql);

        if ($result){
            setNextTeam();
            header('Location: index.php');
        }else{
            echo "Fehler beim Speichern";
        }


    }
}