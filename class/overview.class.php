<?php

class Overview {

    /**
     * Get Data from Array
     *
     * @static
     * @return array
     */
    public static function getTopics(){
        global $conn;
        $data = array();

        $sql = "SELECT name FROM `topics` ORDER BY `order` ASC";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            $data = $result->fetch_all();
        }

        return $data;
    }


    /**
     * Get Data from Array
     *
     * @static
     * @return array

     */
    public static function getQuestions(){
        global $conn;
        $data = array();

        $sql = "SELECT q.id, q.points, q.won_group, t.order FROM questions as q INNER JOIN topics as t ON q.topic = t.id ORDER BY q.points, t.order ASC";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {

                $points = $row["points"];
                $order = $row["order"];
                $id = $row["id"];
                $won_group = $row["won_group"];

                $data[$points][$order]["id"] = $id;
                $data[$points][$order]["points"] = $points;
                $data[$points][$order]["won_group"] = $won_group;
            }
        }

        return $data;
    }


    /**
     * show the overview
     *
     * @static
     */
    public static function showOverview() {
        $topics = self::getTopics();
        $questions = self::getQuestions();

        echo self::drawTable($topics,$questions);
    }


    /**
     * draw the table
     *
     * @param $topics
     * @param $questions
     * @static
     * @return string
     */
    public static function drawTable($topics, $questions){
        $thead_html = "<thead><tr>";

        foreach ($topics as $topic){
            $thead_html .= "<th>".$topic[0]."</th>";
        }
        $thead_html .= "</tr></thead>";

        $tbody_html = "<tbody>";

        foreach ($questions as $points){

            $tbody_html .= "<tr>";
            foreach ($points as $question){
                if ($question['won_group'] != "0"){
                    $tbody_html .= "<td class='locked'>";
                    $tbody_html .= '<a href="#">';
                }else{
                    $tbody_html .= "<td>";
                    $tbody_html .= '<a href="index.php?question='.$question['id'].'">';
                }


                $tbody_html .= $question['points'];
                $tbody_html .= '</a>';

                $tbody_html .= "</td>";
            }
            $tbody_html .= "</tr>";
        }

        $tbody_html .= "</tbody>";

        $html = '<div class="overview"><table>';
        $html .= $thead_html;
        $html .= $tbody_html;
        $html .= '</table></div>';

        return $html;
    }


}