<?php

class Team {

    private $id;
    private $name;
    private $color;

    public function __construct($id)
    {
        $this->id = $id;
        $this->getData();
    }


    private function getData(){
        global $conn;

        $sql = "SELECT * FROM teams WHERE id = " . $this->id;
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $this->id = $row["id"];
                $this->name = $row["name"];
                $this->color = $row["color"];
            }
        }
    }

    public function getColor(){
        return $this->color;
    }

    public function getName(){
        return $this->name;
    }

    public function getID(){
        return $this->id;
    }


    public function getPoints(){
        global $conn;
        $points = 0;
        if (!empty($this->id)){
            $sql = "SELECT SUM(questions.points) AS `sum` FROM `teams` INNER JOIN questions ON teams.id = questions.won_group WHERE teams.id = ".$this->id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {

                    if (!empty($row["sum"])){
                        $points = $row["sum"];
                    }

                }
            }
        }
        return $points;
    }



}