<?php

$teams = getTeams();

if (!isset($_SESSION['current_team']) || empty($_SESSION['current_team'])){
    $_SESSION['current_team'] = 0;
}

function getTeams(){
    global $conn;
    $teams = array();

    $sql = "SELECT id FROM teams";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $id = $row['id'];
            $teams[] = new Team($id);
        }
    }

    return $teams;
}

function setNextTeam(){
    global $teams;

    $current = $_SESSION['current_team'];
    $next = $current + 1;
    $count = sizeof($teams) -1;

    if ($next > $count){
        $next = 0;
    }

    $_SESSION['current_team'] = $next;
}



