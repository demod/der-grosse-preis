<?php


function clearGame(){
    //Aufruf via ?newgame=1
    global $conn;

    $sql = "UPDATE questions SET won_group = 0";
    $result = $conn->query($sql);

    if ($result){
        session_destroy();
        header('Location: index.php');
    }else{
        echo "Fehler beim Löschen";
    }

}