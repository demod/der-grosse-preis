-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 16. Mrz 2017 um 17:44
-- Server Version: 5.6.16
-- PHP-Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `der_grosse_preis`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `statement` text NOT NULL,
  `topic` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `won_group` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Daten für Tabelle `questions`
--

INSERT INTO `questions` (`id`, `question`, `answer`, `statement`, `topic`, `points`, `won_group`) VALUES
(1, 'Frage 1', 'Antwort', 'Das ist halt so', 1, 100, 0),
(2, 'Frage 2', 'Antwort', 'Das ist halt so', 1, 200, 0),
(3, 'Frage 3', 'Antwort', 'Das ist halt so', 1, 300, 1),
(4, 'Frage 4', 'Antwort', 'Das ist halt so', 1, 400, 0),
(5, 'Frage 5', 'Antwort', 'Das ist halt so', 1, 500, 0),
(6, 'Frage 1', 'Antwort', 'Das ist halt so', 2, 100, 0),
(7, 'Frage 2', 'Antwort', 'Das ist halt so', 2, 200, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `teams`
--

INSERT INTO `teams` (`id`, `name`) VALUES
(1, 'Gruppe 1'),
(2, 'Gruppe 2');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `order` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `topics`
--

INSERT INTO `topics` (`id`, `name`, `order`) VALUES
(1, 'Thema A', 1),
(2, 'Thema B', 2),
(3, 'Thema C', 3),
(4, 'Thema D', 4),
(5, 'Thema E', 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
