var outOfDate = false;
var url = window.location.href;

$.fn.countdown = function (callback, duration, message) {
    message = message || "";
    var container = $(this[0]).html(duration + message);
    var countdown = setInterval(function () {
        if (--duration) {
            container.html(duration + message);
        } else {
            clearInterval(countdown);
            callback.call(container);
        }
    }, 1000);

};


$(".countdown").countdown(outofDate, 30, " Sek.");

function outofDate () {
    outOfDate = true;
    this.html('Zeit ist abgelaufen');

}

$(document).ready(function(){
    $('body').keypress(function(e){
        if (e.which == 13) { //ENTER
            if (outOfDate){
                $('.countdown').hide();
                $('.answer').show();
                $('.statement').show();
            }
        }

        if (e.which == 49) { //1
            if (outOfDate){
                document.location.href = url+"&action=save&team=1";
            }
        }

        if (e.which == 50) { //2
            if (outOfDate){
                document.location.href = url+"&action=save&team=2";
            }
        }

        if (e.which == 51) { //3
            if (outOfDate){
                document.location.href = url+"&action=save&team=3";
            }
        }

        if (e.which == 52) { //4
            if (outOfDate){
                document.location.href = url+"&action=save&team=4";
            }
        }

        if (e.which == 48) { //0
            if (outOfDate){
                document.location.href = url+"&action=save&team=none";
            }
        }

    });
});